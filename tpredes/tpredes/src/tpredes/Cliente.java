
package tpredes;

import java.io.*;
import java.net.*;
import java.util.Scanner;
public class Cliente implements Runnable{

    private Socket cliente;

    public Cliente(Socket cliente){
        this.cliente = cliente;
    }

    public static void main(String args[]) throws UnknownHostException, IOException {

        // para se conectar ao servidor, cria-se objeto Socket.
        // O primeiro par�metro � o IP ou endere�o da m�quina que
        // se quer conectar e o segundo � a porta da aplica��o.
        // Neste caso, usa-se o IP da m�quina local (127.0.0.1)
        // e a porta da aplica��o ServidorDeEco (12345).
        Socket socket = new Socket("200.239.176.79", 12345);

        /*Cria um novo objeto Cliente com a conex�o socket para que seja executado em um novo processo.
        Permitindo assim a conex�o de v�rio clientes com o servidor.*/
      
            Cliente c = new Cliente(socket);
            Thread t = new Thread(c);
            t.start();
                   
    }

    public void run() {
        try {
             
            System.out.println("O cliente conectou ao servidor");

            //Cria  objeto para enviar a mensagem ao servidor
            PrintStream saida = new PrintStream(this.cliente.getOutputStream());

            //Envia mensagem ao servidor
            String labirinto = "INI *** 000 000 000 000 000 000 000 000 000 000 *** 000 *** *** *** *** *** *** *** 000 000 *** 000 000 000 *** 000 *** *** *** 000 000 *** 000 000 *** 000 000 000 *** *** 000 000 *** 000 *** 000 000 000 000 000 *** 000 *** 000 *** 000 000 000 000 000 000 000 *** 000 *** 000 000 000 *** *** *** *** *** 000 *** *** *** 000 000 000 000 000 000 000 000 000 000 *** *** *** *** *** *** *** *** *** FIM";          
            saida.println(labirinto);          
            
            Scanner s = null;
            s = new Scanner(this.cliente.getInputStream());
            //imprimelabirinto(s.nextLine());
           
            
              String[][] labirintoMat = null;

            if(labirintoMat == null){
                try{
                    labirintoMat = lerCriarLabirinto(s.nextLine());
                }
                catch(IOException e){
                    throw new RuntimeException(e);
                }
            }
            imprimeLabirinto(labirintoMat);
            this.cliente.close();
            System.out.println("Fim do cliente!");
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

     public String[][] lerCriarLabirinto(String labs) throws IOException {    
        Reader inputString = new StringReader(labs);
        BufferedReader bi = new BufferedReader(inputString);
        String lab[][] = new String[10][10];
        String[] strNums;

        strNums = bi.readLine().split("\\s");
        int k = 0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++)
                lab[i][j] = strNums[k++];
        }
        lab[0][0] = "INI";
        return lab;
    }

    public static void imprimeLabirinto(String[][] matriz) {
        //matriz[0][0] = "INI";
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                System.out.print(matriz[i][j]);
                System.out.print(" ");
            }
            System.out.print("\n");
        }
        System.out.print("\n");
    }

}

/*
public class Cliente {
        public static void main(String args[]) throws Exception {
        Socket sk= new Socket("127.0.0.1", 30010);
        sk.setKeepAlive(true);
        BufferedReader sin = new BufferedReader(new InputStreamReader(sk.getInputStream()));
        PrintStream sout = new PrintStream(sk.getOutputStream());
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        //String st;
        int cond = 0;
        Scanner st;
        while(cond < 1)
        {
            Solicita solicitaConeccao = new Solicita(sk);
            Thread bot = new Thread(solicitaConeccao);
            System.out.println("aqui cliente: "+sk.isClosed());
            bot.start();
            while (true) {
                System.out.print(" cliente aguardando ");
                System.out.println("aqui cliente: "+sk.isClosed());
                
                InputStream istream = sk.getInputStream();
                
                BufferedReader receiveRead = new BufferedReader(new InputStreamReader(istream));
                st = new Scanner(receiveRead.readLine());
                System.out.println("aqui cliente: "+sk.isConnected());
                System.out.print("Server : " + st + "\n");
                break;
 
            }
            cond++;
        }
    }
}
*/

