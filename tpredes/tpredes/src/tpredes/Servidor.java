
package tpredes;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
//import java.tpredes.src.tpredes.tratamento.java;
public class Servidor implements Runnable{
    public Socket cliente;
    public String labirinto;
    static int cont=0;

    public Servidor(Socket cliente){
        this.cliente = cliente;
    }

    public static void main(String[] args)  throws IOException{     

        //Cria um socket na porta 12345
        ServerSocket servidor = new ServerSocket (12345);
        System.out.println("Porta 12345 aberta!");

        // Aguarda algu�m se conectar. A execu��o do servidor
        // fica bloqueada na chamada do m�todo accept da classe
        // ServerSocket. Quando algu�m se conectar ao servidor, o
        // m�todo desbloqueia e retorna com um objeto da classe
        // Socket, que � uma porta da comunica��o.
        System.out.println("Aguardando conex�o do cliente...");   

        while (true) {
          Socket cliente = servidor.accept();
          // Cria uma thread do servidor para tratar a conex�o
          Servidor tratamento = new Servidor(cliente);
          Thread t = new Thread(tratamento);
          // Inicia a thread para o cliente conectado
          t.start();
          System.out.println(cont++); 
           
        }
    }

    /* A classe Thread, que foi instancia no servidor, implementa Runnable.
       Ent�o voc� ter� que implementar sua l�gica de troca de mensagens dentro deste m�todo 'run'.
    */
    public void run(){
        System.out.println("Nova conexao com o cliente " + this.cliente.getInetAddress().getHostAddress());

        try {
            Scanner s = null;
            s = new Scanner(this.cliente.getInputStream());
            
                 
           //Exibe mensagem no console
           
            labirinto =  s.nextLine();
   
              
            String[][] labirintoMat = null;

            if(labirintoMat == null){
                try{
                    labirintoMat = lerCriarLabirinto();
                }
                catch(IOException e){
                    throw new RuntimeException(e);
                }
            }

            System.out.println("Labirinto inicial: \n");

            if (achaCaminho(9, 9, 10, 10, labirintoMat)) {
               System.out.println("Caminho encontrado com sucesso!\n");
            } else
                System.out.println("Caminho nao encontrado\n");

            String retorno = matrixToString(labirintoMat);
            
             
             
             PrintStream saida = new PrintStream(this.cliente.getOutputStream());

            //Envia mensagem ao cliente
            saida.println(retorno);  
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
   public String matrixToString(String[][] matriz){
        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            for(int j = 0; j < 10; j++) {
                strBuilder.append(matriz[i][j]);
                strBuilder.append(" ");
            }
        }

        String a = strBuilder.toString();

        return a;
    }

    public String[][] lerCriarLabirinto() throws IOException {    
        Reader inputString = new StringReader(labirinto);
        BufferedReader bi = new BufferedReader(inputString);
        String lab[][] = new String[10][10];
        String[] strNums;

        strNums = bi.readLine().split("\\s");
        int k = 0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++)
                lab[i][j] = strNums[k++];
        }
        lab[0][0] = "INI";
        return lab;
    }
     public String[][] lerCriarLabirinto(String labs) throws IOException {    
        Reader inputString = new StringReader(labs);
        BufferedReader bi = new BufferedReader(inputString);
        String lab[][] = new String[10][10];
        String[] strNums;

        strNums = bi.readLine().split("\\s");
        int k = 0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++)
                lab[i][j] = strNums[k++];
        }
        lab[0][0] = "INI";
        return lab;
    }

    public static void imprimeLabirinto(String[][] matriz) {
        //matriz[0][0] = "INI";
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                System.out.print(matriz[i][j]);
                System.out.print(" ");
            }
            System.out.print("\n");
        }
        System.out.print("\n");
    }

    private static Boolean achaCaminho(int linhaAtual, int colunaAtual, int linhas, int colunas, String[][] labirinto) {
        //condi??o para n?o ultrapassar os limites do mapa
        if (linhaAtual < 0 || linhaAtual > linhas - 1 || colunaAtual < 0 || colunaAtual > colunas - 1) {
            return false;
        }

        //condi??o de parada - achou o in?cio
        if (labirinto[linhaAtual][colunaAtual].contentEquals("INI")) {
            return true;
        }

        //encontrou parede
        if ((!labirinto[linhaAtual][colunaAtual].contentEquals("000")) && (!labirinto[linhaAtual][colunaAtual].contentEquals("FIM"))) {
            return false;
        }

        //preenche com caminho vazio
        labirinto[linhaAtual][colunaAtual] = "___";

        //vai para o oeste
        if (achaCaminho(linhaAtual, colunaAtual - 1, linhas, colunas, labirinto)) {
            return true;
        }

        //vai para o sul
        if (achaCaminho(linhaAtual + 1, colunaAtual, linhas, colunas, labirinto)) {
            return true;
        }

        //vai para o leste
        if (achaCaminho(linhaAtual, colunaAtual + 1, linhas, colunas, labirinto)) {
            return true;
        }

        //vai para o norte
        if (achaCaminho(linhaAtual - 1, colunaAtual, linhas, colunas, labirinto)) {
            return true;
        }

        //quando desfaz o caminho percorrido
        labirinto[linhaAtual][colunaAtual] = "000";
        return false;
    }
}
